import display.TreeDisplay;
import entities.NodesResource;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        FileLoader fileLoader = new FileLoader();
        NodesResource nodes = fileLoader.getNodes("test1.xlsx");
        nodes.print();

        primaryStage.setTitle("DataReader");
        TreeDisplay tree = new TreeDisplay();
        primaryStage.setScene(tree.display(nodes));
        primaryStage.show();
    }
}