package entities;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Node {

    private int id;
    private String name;
    private List<Node> nodes = new ArrayList<>();

    public Node() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public void addChild(Node node) {
        this.nodes.add(node);
    }

    @Override
    public String toString() {

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = null;

        try {
            json = ow.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }
}
