import entities.Node;
import entities.NodesResource;
import javafx.util.Pair;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FileLoader {

    private XSSFSheet load(String name)  {

       FileInputStream fis = null;
       XSSFWorkbook workbook = null;

       try {
           fis = new FileInputStream(getFileFromResource(name));
       } catch (FileNotFoundException e) {
           System.out.println("Error: loading file failed ");
       }

       try {
           workbook = new XSSFWorkbook(fis);
       } catch (IOException e) {
           System.out.println("Error: loading file failed");
       }

       return workbook.getSheetAt(0);
    }

    public NodesResource getNodes(String file) {
      return extractNodes(load(file));
    }

    private File getFileFromResource(String name) {
        URL url = getClass().getResource(name);

        if(url == null) {
            System.out.println("Error: loading file failed ");
            System.exit(0);
        }
        return new File(url.getPath());
    }

    private Iterator<Row> prepareIterator(Iterator<Row> rowIt) {
        rowIt.next();
        return rowIt;
    }

    private NodesResource extractNodes(XSSFSheet sheet) {

        Iterator<Row> rowIterator = prepareIterator(sheet.iterator());
        List<Pair<Node,Integer>> list = createNodeLevelList(rowIterator);
        list = setChildren(list);

        return pairListToNodeList(list);
    }

    private List<Pair<Node,Integer>> createNodeLevelList(Iterator<Row> rowIterator) {

        List<Pair<Node,Integer>> list = new ArrayList<>();

        while (rowIterator.hasNext()) {
            list.add(createRowLevelPair(rowIterator.next()));
        }

        return list;
    }

    private Pair<Node, Integer> createRowLevelPair(Row row) {

        Iterator<Cell> cellIterator = row.cellIterator();
        Node newNode = new Node();
        int count = 0, level = 0;

        while (cellIterator.hasNext()) {
            count++;
            Cell cell = cellIterator.next();

            if(!cell.toString().isEmpty() && cellIterator.hasNext()) {
                newNode.setName(cell.toString());
                level = count;
            }
            else if(!cell.toString().isEmpty()) {
                newNode.setId((int)(Double.parseDouble(cell.toString())));
            }

        }

        return  new Pair<>(newNode,level);
    }

    private List<Pair<Node,Integer>> setChildren(List<Pair<Node,Integer>> list) {

        ArrayList<Node> levelList = new ArrayList<>();

        for (Pair<Node,Integer> pair: list) {

            if(pair.getValue() == 1) {
                levelList.clear();
            }
            else {
                levelList.get(pair.getValue() - 2).addChild(pair.getKey());
            }

            levelList.add(pair.getValue() -1, pair.getKey());
        }

        return list;
    }

    private NodesResource pairListToNodeList(List<Pair<Node,Integer>> list) {

        NodesResource nodes = new NodesResource();
        list.forEach(x -> nodes.add(x.getKey()));

        return nodes;
    }


}
