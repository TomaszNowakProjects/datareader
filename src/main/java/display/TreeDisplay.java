package display;

import entities.Node;
import entities.NodesResource;
import javafx.scene.Scene;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.StackPane;

public class TreeDisplay {

    public Scene display(NodesResource nodes) {

        StackPane tree = new StackPane(createTreeView(nodes));
        return new Scene(tree, 300, 350);
    }

    private StackPane createTreeView(NodesResource nodes) {

        TreeItem<String> rootItem = new TreeItem<> ("Tree");
        rootItem.setExpanded(true);
        StackPane root = new StackPane();

        rootItem = addAllNodes(rootItem, nodes);

        TreeView<String> tree = new TreeView<> (rootItem);
        root.getChildren().add(tree);
        return root;
    }

    private TreeItem<String> addNode(Node node) { //todo - test

        TreeItem<String> Item = new TreeItem<>(node.getName());

        node.getNodes()
                .stream()
                .forEach(n -> Item.getChildren().add(addNode(n)));

        return Item;
    }

    private TreeItem<String> addAllNodes(TreeItem<String> rootItem, NodesResource nodes) {

        nodes.stream()
                .filter(n -> n.getName().length() == 1)
                .forEach(n -> rootItem.getChildren().add(addNode(n)));

        return rootItem;
    }
}
