package entities;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class NodeTest {

    @Test
    public void shouldAddNode() {

        //given
        Node node = new Node();
        Node child = new Node();
        List<Node> list = new ArrayList<>();
        list.add(child);

        //when
        node.addChild(child);

        //then
        assertEquals(list, node.getNodes());

    }
}
