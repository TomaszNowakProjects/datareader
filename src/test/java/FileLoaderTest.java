import entities.Node;
import entities.NodesResource;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FileLoaderTest {

    @Test
    public void shouldGetNodes() {

        //given
        Node nodeA = new Node();
        nodeA.setName("A");
        nodeA.setId(1);
        Node nodeAA = new Node();
        nodeAA.setName("AA");
        nodeAA.setId(2);
        Node nodeAA1 = new Node();
        nodeAA1.setName("AA1");
        nodeAA1.setId(3);
        Node nodeAA2 = new Node();
        nodeAA2.setName("AA2");
        nodeAA2.setId(4);
        Node nodeAB = new Node();
        nodeAB.setName("AB");
        nodeAB.setId(5);
        Node nodeB = new Node();
        nodeB.setName("B");
        nodeB.setId(6);
        Node nodeC = new Node();
        nodeC.setName("C");
        nodeC.setId(7);
        Node nodeCA = new Node();
        nodeCA.setName("CA");
        nodeCA.setId(8);
        Node nodeCA1 = new Node();
        nodeCA1.setName("CA1");
        nodeCA1.setId(9);
        Node nodeCA2 = new Node();
        nodeCA2.setName("CA2");
        nodeCA2.setId(10);
        Node nodeD = new Node();
        nodeD.setName("D");
        nodeD.setId(11);
        Node nodeDA = new Node();
        nodeDA.setName("DA");
        nodeDA.setId(12);

        nodeA.addChild(nodeAA);
        nodeA.addChild(nodeAB);
        nodeAA.addChild(nodeAA1);
        nodeAA.addChild(nodeAA2);
        nodeC.addChild(nodeCA);
        nodeCA.addChild(nodeCA1);
        nodeCA.addChild(nodeCA2);
        nodeD.addChild(nodeDA);

        NodesResource nodes = new NodesResource();
        Node[] list = new Node[] {nodeA, nodeAA, nodeAA1, nodeAA2, nodeAB,
                    nodeB, nodeC, nodeCA, nodeCA1, nodeCA2, nodeD, nodeDA};
        nodes.addAll(Arrays.asList(list));

        FileLoader fileLoader = new FileLoader();


        //when
        NodesResource getNodes = fileLoader.getNodes("test1.xlsx");

        //then
        assertEquals(nodes.toString(), getNodes.toString());

    }

}
